import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class Student_Mark_DAO {

    private final Session session;

    public Student_Mark_DAO(Session session) {
        this.session = session;
    }

    public void saveStudent(Student student, Mark mark) {
        session.save(student);
        session.save(mark);
    }

    public void updateStudent(Student student) {
        Query query = session.createQuery("UPDATE Student SET name = :name, email = :email WHERE regno = :regno");
        query.setParameter("name", student.getName());
        query.setParameter("email", student.getEmail());
        query.setParameter("regno", student.getRegno());
        query.executeUpdate();
    }
    public void updateMark(Mark mark) {
        Query query = session.createQuery("UPDATE Mark SET subject1 = :subject1, subject2 = :subject2, subject3 = :subject3 WHERE regno = :regno");
        query.setParameter("subject1", mark.getSubject1());
        query.setParameter("subject2", mark.getSubject2());
        query.setParameter("subject3", mark.getSubject3());
        query.setParameter("regno", mark.getRegno());
        query.executeUpdate();
    }

    public void updateBothStudentAndMark(Student student, Mark mark) {
//       use query to update student details
        Query query = session.createQuery("UPDATE Student SET name = :name, email = :email WHERE regno = :regno");
        query.setParameter("name", student.getName());
        query.setParameter("email", student.getEmail());
        query.setParameter("regno", student.getRegno());
        query.executeUpdate();
//        use query to update mark details
        Query query1 = session.createQuery("UPDATE Mark SET subject1 = :subject1, subject2 = :subject2, subject3 = :subject3 WHERE regno = :regno");
        query1.setParameter("subject1", mark.getSubject1());
        query1.setParameter("subject2", mark.getSubject2());
        query1.setParameter("subject3", mark.getSubject3());
        query1.setParameter("regno", mark.getRegno());
        query1.executeUpdate();
    }

    public List<Student> getStudentByRegno(long regno) {
//        create an active session
        Query query = session.createQuery("FROM Student WHERE regno = :regno");
        query.setParameter("regno", regno);
        return query.getResultList();

    }

    public void deleteStudent(Student student) {
        session.delete(student);
//        delete mark with the same regno
        Query query = session.createQuery("DELETE FROM Mark WHERE regno = :regno");
        query.setParameter("regno", student.getRegno());
        query.executeUpdate();
    }

    public List<Student> getAllStudents() {
        Query<Student> query = session.createQuery("FROM Student", Student.class);
        return query.list();
    }

    public List<Mark> getMarkById(long id) {
//        get mark details by regno
        Query query = session.createQuery("FROM Mark WHERE regno = :regno");
        query.setParameter("regno", id);

        return  query.getResultList();
    }
}

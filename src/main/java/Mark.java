import javax.persistence.*;

@Entity
@Table(name = "mark")
public class Mark {
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private Long id;

    public long getRegno() {
        return regno;
    }

    public void setRegno(long regno) {
        this.regno = regno;
    }

    @Id
    private long regno;
    @Column(name = "subject1")
    private int subject1;

    @Column(name = "subject2")
    private int subject2;

    @Column(name = "subject3")
    private int subject3;


    // Constructors, getters, setters, etc.

    public Mark() {
    }

    public Mark(long regno, int subject1, int subject2, int subject3) {
        this.regno = regno;
        this.subject1 = subject1;
        this.subject2 = subject2;
        this.subject3 = subject3;
    }

    public int getSubject1() {
        return subject1;
    }

    public void setSubject1(int subject1) {
        this.subject1 = subject1;
    }

    public int getSubject2() {
        return subject2;
    }

    public void setSubject2(int subject2) {
        this.subject2 = subject2;
    }

    public int getSubject3() {
        return subject3;
    }

    public void setSubject3(int subject3) {
        this.subject3 = subject3;
    }



}


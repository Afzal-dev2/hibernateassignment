import javax.persistence.*;

@Entity
@Table(name = "student")
public class Student {
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @Column(name = "id")
//    private int id;

    @Id
    private Long regno;
    @Column(name = "name")
    private String name;

    @Column(name = "email")
    private String email;

    public Student() {
    }

    public Student(long regNo, String name, String email) {
        this.regno = regNo;
        this.name = name;
        this.email = email;
    }

    public Long getRegno() {
        return regno;
    }

    public void setRegno(Long regno) {
        this.regno = regno;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}

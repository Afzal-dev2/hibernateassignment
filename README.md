# Student Mark Management System

## Description
This project is a simple Java application for managing student and mark data using Hibernate for ORM (Object-Relational Mapping). It provides CRUD (Create, Read, Update, Delete) operations for students and their associated marks.

## Modules

### 1. `Main` Class
- The entry point of the application.
- Handles user input and menu-driven interaction.

### 2. `Student` Class
- Represents a student entity mapped to the "student" table in the database.
- Includes fields for registration number, name, and email.

### 3. `Mark` Class
- Represents a mark entity mapped to the "mark" table in the database.
- Includes fields for registration number and marks in three subjects.

### 4. `Student_Mark_DAO` Class
- Data Access Object (DAO) class for performing CRUD operations on student and mark entities.
- Provides methods for saving, updating, retrieving, and deleting student and mark data from the database.

### 5. `Crud` Class
- Handles CRUD operations by interacting with the `Student_Mark_DAO` class.
- Provides methods for inserting, updating, displaying, and deleting student and mark data.
- Includes a utility method for printing messages in highlighted yellow color.

## Prerequisites
- Java Development Kit (JDK) 17
- Maven
- XAMPP or any other MySQL server
- IntelliJ IDEA or any other Java IDE

## Setup
1. Clone the repository.
2. Import the project into your preferred IDE.
3. Configure the MySQL database settings in the `hibernate.cfg.xml` file.
4. Build the project using Maven.
5. Run the `Main` class to start the application.

## Usage
- Upon running the application, a menu will be displayed with options to perform various operations:
    1. Insert student and mark data
    2. Update student and mark data
    3. Display student and mark data
    4. Delete a particular student and mark data
    5. View all student and mark data
    6. Exit

- Choose the desired option by entering the corresponding number.
- Follow the prompts to input data or perform actions as required.

[//]: # (include the relative path to the image in the repository)
![Screenshot1](/screenshots/sc1.png)
![Screenshot2](/screenshots/sc2.png)
![Screenshot3](/screenshots/sc3.png)
![Screenshot4](/screenshots/sc4.png)
![Screenshot5](/screenshots/sc5.png)
![Screenshot6](/screenshots/sc6.jpeg)
![Screenshot7](/screenshots/sc7.png)
![Screenshot8](/screenshots/sc8.png)
![Screenshot9](/screenshots/sc9.png)
![Screenshot10](/screenshots/sc10.png)

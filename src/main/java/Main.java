import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import java.util.Scanner;
public class Main {

    public static void main(String[] args) {
        Configuration configuration = new Configuration().configure();
        SessionFactory sessionFactory = configuration.buildSessionFactory();
        Session session = sessionFactory.getCurrentSession();
        Crud crud = new Crud(session);
        Student_Mark_DAO studentDAO = new Student_Mark_DAO(session);
        session.beginTransaction();
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("\nMenu:");
            System.out.println("1. Insert student and mark data");
            System.out.println("2. Update student data");
            System.out.println("3. Update student's mark data");
            System.out.println("4. Update both student and mark data");
            System.out.println("5. Display student and mark data");
            System.out.println("6. Delete a particular student and mark data");
            System.out.println("7. View all student and mark data");
            System.out.println("8. Exit");
            System.out.print("Enter your choice: ");
            int choice = scanner.nextInt();

            switch (choice) {
                case 1:
                    crud.insertStudentAndMark(scanner, studentDAO);
                    break;
                case 2:
                    crud.updateStudent(scanner, studentDAO);
                    break;
                case 3:
                    crud.updateStudentMark(scanner, studentDAO);
                    break;
                case 4:
                    crud.updateStudentAndMark(scanner, studentDAO);
                case 5:
                    crud.displayStudentAndMark(scanner, studentDAO);
                    break;
                case 6:
                   crud.deleteStudentAndMark(scanner, studentDAO);
                    break;
                case 7:
                    crud.viewAllStudentAndMark();
                    break;
                case 8:
                    System.out.println("Exiting program...");
                    session.getTransaction().commit();
                    sessionFactory.close();
                    return;
                default:
                    System.out.println("Invalid choice. Please enter a number between 1 and 5.");
            }
        }
    }



}

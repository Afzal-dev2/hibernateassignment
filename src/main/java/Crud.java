import org.hibernate.NonUniqueObjectException;
import org.hibernate.Session;

import java.util.List;
import java.util.Scanner;

public class Crud {
    private final Session session;

    public Crud(Session session) {
        this.session = session;
    }
    public  void insertStudentAndMark(Scanner scanner, Student_Mark_DAO studentDAO) {
       try{
           System.out.println();
           printHighlighted("Enter student details:");
           System.out.print("Register number: ");
           long regno = scanner.nextLong();
           scanner.nextLine();
           System.out.print("Name: ");
           String name = scanner.nextLine();
           System.out.print("Email: ");
           String email = scanner.nextLine();
           System.out.print("Subject 1 marks: ");
           int subject1 = scanner.nextInt();
           System.out.print("Subject 2 marks: ");
           int subject2 = scanner.nextInt();
           System.out.print("Subject 3 marks: ");
           int subject3 = scanner.nextInt();
//
           Student student = new Student(regno,name, email);
           Mark mark = new Mark(regno, subject1, subject2, subject3);
//           session.beginTransaction();
           studentDAO.saveStudent(student, mark);
//           session.getTransaction().commit();

           printHighlighted("Student and marks data inserted successfully.");
       }
         catch (NonUniqueObjectException e) {
             printHighlightedRed("Student and marks data already exists!!");
         }
    }

    public  void updateStudentAndMark(Scanner scanner, Student_Mark_DAO studentDAO) {
        try{
            printHighlighted("Update student details:");
            System.out.print("Register number to update: ");
            long regno = scanner.nextLong();
            scanner.nextLine();
        //        check if the given student data exists
        //        session.beginTransaction();

            if (!isStudentExists(regno, studentDAO)) {
                printHighlightedRed("No student found.");
                return;
            }

            System.out.print("Name: ");
            String name = scanner.nextLine();
            System.out.print("Email: ");
            String email = scanner.nextLine();
            System.out.print("Subject 1 marks: ");
            int subject1 = scanner.nextInt();
            System.out.print("Subject 2 marks: ");
            int subject2 = scanner.nextInt();
            System.out.print("Subject 3 marks: ");
            int subject3 = scanner.nextInt();

            Student student = new Student(regno,name, email);
            Mark mark = new Mark(regno, subject1, subject2, subject3);
        //        list of students and marks details
            System.out.println("Below Student details are inserted:");
            System.out.println("Register number: " + student.getRegno());
            System.out.println("Name: " + student.getName());
            System.out.println("Email: " + student.getEmail());
            System.out.println("Subject 1: " + mark.getSubject1());
            System.out.println("Subject 2: " + mark.getSubject2());
            System.out.println("Subject 3: " + mark.getSubject3());
            studentDAO.updateBothStudentAndMark(student, mark);
//            session.getTransaction().commit();
            printHighlighted("Student and marks data updated successfully.");
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public  void displayStudentAndMark(Scanner scanner, Student_Mark_DAO studentDAO) {
        try{
            System.out.print("Register number: ");
            long regno = scanner.nextLong();
            scanner.nextLine();
//            session.beginTransaction();
            List<Student> student = studentDAO.getStudentByRegno(regno);
            List<Mark> mark = studentDAO.getMarkById(regno);

            if (!isStudentExists(regno, studentDAO)) {
                printHighlightedRed("No student and mark data found.");
                return;
            }
            System.out.println("Student details:");
            for (Student s : student) {
                printHighlighted("Register number: " + s.getRegno());
                printHighlighted("Name: " + s.getName());
                printHighlighted("Email: " + s.getEmail());

            }
            System.out.println("Mark details:");
            for (Mark m : mark) {
                printHighlighted("Subject 1: " + m.getSubject1());
                printHighlighted("Subject 2: " + m.getSubject2());
                printHighlighted("Subject 3: " + m.getSubject3());
            }
//            session.getTransaction().commit();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public  void deleteStudentAndMark(Scanner scanner, Student_Mark_DAO studentDAO) {
        try {
//            session.beginTransaction();
            System.out.print("Register number: ");
            long regno = scanner.nextLong();
            scanner.nextLine();
            List<Student> students = studentDAO.getStudentByRegno(regno);
            if (students.isEmpty()) {
                printHighlightedRed("No student and mark data found.");
                return;
            }
            studentDAO.deleteStudent(students.get(0));
//            session.getTransaction().commit();
            printHighlighted("Student and mark data deleted successfully.");

        } catch (Exception e) {
            e.printStackTrace();
        }
        }

    public void viewAllStudentAndMark() {
//        display all students and mark data
//        session.beginTransaction();
        Student_Mark_DAO studentDAO = new Student_Mark_DAO(session);
        List<Student> students = studentDAO.getAllStudents();

        if (!isStudentExists(students.get(0).getRegno(), studentDAO)) {
            printHighlightedRed("No student and mark data found.");
            return;
        }
        for (Student s : students) {
            printHighlighted("Register number: " + s.getRegno());
            printHighlighted("Name: " + s.getName());
            printHighlighted("Email: " + s.getEmail());
            List<Mark> marks = studentDAO.getMarkById(s.getRegno());
            for (Mark m : marks) {
                printHighlighted("Subject 1: " + m.getSubject1());
                printHighlighted("Subject 2: " + m.getSubject2());
                printHighlighted("Subject 3: " + m.getSubject3());
            }
            System.out.println("------------------------------------------------");
        }
//        session.getTransaction().commit();


    }
//    print highlighted in yellow
    public static void printHighlighted(String message) {
        String ANSI_YELLOW = "\u001B[33m";
        // ANSI escape code for reset (back to default) text color
        String ANSI_RESET = "\u001B[0m";

        System.out.println(ANSI_YELLOW + message + ANSI_RESET);
    }
    public static void printHighlightedRed(String message) {
        String ANSI_YELLOW = "\u001B[31m";
        // ANSI escape code for reset (back to default) text color
        String ANSI_RESET = "\u001B[0m";

        System.out.println(ANSI_YELLOW + message + ANSI_RESET);
    }

    public void updateStudent(Scanner scanner, Student_Mark_DAO studentDAO) {
        try{
            printHighlighted("Update student details:");
            System.out.print("Enter Register number to update: ");
            long regno = scanner.nextLong();
            scanner.nextLine();
            //        check if the given student data exists
            //        session.beginTransaction();
            List<Student> students = studentDAO.getStudentByRegno(regno);
           if (!isStudentExists(regno, studentDAO)) {
                printHighlightedRed("No student found.");
                return;
            }
            System.out.print("Name: ");
            String name = scanner.nextLine();
            System.out.print("Email: ");
            String email = scanner.nextLine();

            Student student = new Student(regno,name, email);
            //        list of students and marks details
            System.out.println("Below Student details are inserted:");
            System.out.println("Register number: " + student.getRegno());
            System.out.println("Name: " + student.getName());
            System.out.println("Email: " + student.getEmail());
            studentDAO.updateStudent(student);
//            session.getTransaction().commit();
            printHighlighted("Student data updated successfully.");
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateStudentMark(Scanner scanner, Student_Mark_DAO studentDAO) {
        try{
            printHighlighted("Update student's Mark details:");
            System.out.print("Enter Register number to update: ");
            long regno = scanner.nextLong();
            scanner.nextLine();
            //        check if the given student data exists
            //        session.beginTransaction();
            List<Student> students = studentDAO.getStudentByRegno(regno);

            if (!isStudentExists(regno, studentDAO)) {
                printHighlightedRed("No student found.");
                return;
            }
            System.out.print("Subject 1 marks: ");
            int subject1 = scanner.nextInt();
            System.out.print("Subject 2 marks: ");
            int subject2 = scanner.nextInt();
            System.out.print("Subject 3 marks: ");
            int subject3 = scanner.nextInt();


            Mark mark = new Mark(regno, subject1, subject2, subject3);
            //        list of students and marks details
            System.out.println("Below Student marks are inserted:");
            printHighlighted("Register number: " + mark.getRegno());
            System.out.println("Subject 1: " + mark.getSubject1());
            System.out.println("Subject 2: " + mark.getSubject2());
            System.out.println("Subject 3: " + mark.getSubject3());
            studentDAO.updateMark(mark);
//            session.getTransaction().commit();
            printHighlighted("Student's marks updated successfully.");
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isStudentExists(long regno, Student_Mark_DAO studentDAO) {
        List<Student> students = studentDAO.getStudentByRegno(regno);
        return !students.isEmpty();
    }
}
